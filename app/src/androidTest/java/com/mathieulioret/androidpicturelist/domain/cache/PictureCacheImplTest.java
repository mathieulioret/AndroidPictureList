package com.mathieulioret.androidpicturelist.domain.cache;

import android.support.test.InstrumentationRegistry;

import com.mathieulioret.androidpicturelist.data.entity.PictureEntity;
import com.mathieulioret.androidpicturelist.data.mapper.PictureEntityMapper;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.List;

import io.reactivex.observers.TestObserver;

import static junit.framework.Assert.assertEquals;


public class PictureCacheImplTest {

    PictureCache pictureCache;

    @Mock
    PictureEntityMapper pictureEntityMapper;

    @Before
    public void setUp() throws Exception {
        pictureCache = new PictureCacheImpl(InstrumentationRegistry.getTargetContext(), pictureEntityMapper);
    }

    @Test
    public void isCached() throws Exception {
        //GIVEN

        //WHEN
        boolean actual = pictureCache.isCached();
        //THEN
        assertEquals(false, actual);

    }

    @Test
    public void isModelCached() throws Exception {
        //GIVEN

        //WHEN
        boolean actual = pictureCache.isModelCached();
        //THEN
        assertEquals(false, actual);
    }

    @Test
    public void getCache() throws Exception {
        //GIVEN
        TestObserver<List<PictureEntity>> subscriber = TestObserver.create();
        //WHEN
        pictureCache.getCache().subscribe(subscriber);
        //THEN
        subscriber.assertNoErrors();
    }

}