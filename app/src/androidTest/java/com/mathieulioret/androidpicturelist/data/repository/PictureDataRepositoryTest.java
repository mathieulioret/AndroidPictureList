package com.mathieulioret.androidpicturelist.data.repository;

import android.support.test.InstrumentationRegistry;

import com.mathieulioret.androidpicturelist.data.entity.PictureEntity;
import com.mathieulioret.androidpicturelist.data.mapper.PictureEntityMapper;
import com.mathieulioret.androidpicturelist.domain.cache.PictureCache;
import com.mathieulioret.androidpicturelist.domain.model.Picture;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import io.reactivex.observers.TestObserver;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PictureDataRepositoryTest {

    private static final int pictureNumber = 1;
    private PictureDataRepository pictureDataRepository;
    @Mock
    private
    PictureEntityMapper pictureEntityMapper;

    @Mock
    private
    PictureCache pictureCache;

    @Before
    public void setUp() throws Exception {
        pictureDataRepository = new PictureDataRepository(InstrumentationRegistry.getTargetContext(), pictureCache, pictureEntityMapper);
    }

    @Test
    public void getPictures() throws Exception {

        //GIVEN
        Picture picture = mock(Picture.class);
        List<Picture> pictureList = new ArrayList<>();
        pictureList.add(picture);
        TestObserver<List<Picture>> subscriber = TestObserver.create();
        //WHEN
        when(pictureCache.isCached()).thenReturn(false);
        when(pictureEntityMapper.transform(any(PictureEntity.class))).thenReturn(picture);
        when(pictureEntityMapper.transform(any(Collection.class))).thenReturn(pictureList);
        pictureDataRepository.getPictures().subscribe(subscriber);
        //THEN
        subscriber.assertNoErrors();
        subscriber.assertComplete();
        assertEquals(pictureNumber, subscriber.values().get(0).size());

    }

}