package com.mathieulioret.androidpicturelist.presentation.view.fragment;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.mathieulioret.androidpicturelist.R;
import com.mathieulioret.androidpicturelist.presentation.view.activity.PictureActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class PictureFragmentTest {

    @Rule
    public ActivityTestRule<PictureActivity> activityActivityTestRule = new ActivityTestRule<PictureActivity>(PictureActivity.class);

    @Before
    public void init() {
        activityActivityTestRule.getActivity()
                .getSupportFragmentManager().beginTransaction();
    }

    @Test
    public void recyclerViewIsPresent() throws Exception {
        onView(withId(R.id.pictures)).check(matches(isDisplayed()));
        onView(withId(R.id.pictures)).perform(click());
    }


}