package com.mathieulioret.androidpicturelist.data.mapper;


import android.database.Cursor;
import android.support.annotation.NonNull;
import android.util.Log;

import com.mathieulioret.androidpicturelist.data.entity.PictureEntity;
import com.mathieulioret.androidpicturelist.data.repository.local.SqlLiteDatabaseHandler;
import com.mathieulioret.androidpicturelist.domain.model.Picture;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import static android.content.ContentValues.TAG;


public class PictureEntityMapper {

    @Inject
    public PictureEntityMapper() {
    }

    /**
     * Transform a PictureEntity into Picture
     *
     * @param pictureEntity PictureEntity
     * @return Picture
     */
    public Picture transform(@NonNull PictureEntity pictureEntity) {
        Picture picture = new Picture();
        picture.setId(pictureEntity.getId());
        picture.setAlbumId(pictureEntity.getAlbumId());
        picture.setThumbnailUrl(pictureEntity.getThumbnailUrl());
        picture.setTitle(pictureEntity.getTitle());
        picture.setUrl(pictureEntity.getUrl());

        return picture;
    }

    /**
     * Transform a Picture into PictureEntity
     *
     * @param picture Picture
     * @return PictureEntity
     */
    public PictureEntity transformToPictureEntity(@NonNull Picture picture) {
        PictureEntity pictureEntity = new PictureEntity();
        pictureEntity.setId(picture.getId());
        pictureEntity.setAlbumId(picture.getAlbumId());
        pictureEntity.setThumbnailUrl(picture.getThumbnailUrl());
        pictureEntity.setTitle(picture.getTitle());
        pictureEntity.setUrl(picture.getUrl());
        return pictureEntity;
    }

    /**
     * Transform a Collection of PictureEntity into a List<Picture>
     *
     * @param pictureEntityCollection Collection<PictureEntity>
     * @return List<Picture>
     */
    public List<Picture> transform(Collection<PictureEntity> pictureEntityCollection) {
        final List<Picture> pictureList = new ArrayList<>();
        for (PictureEntity pictureEntity : pictureEntityCollection) {
            final Picture picture = transform(pictureEntity);
            if (picture != null) {
                pictureList.add(picture);
            }
        }
        return pictureList;
    }

    /**
     * Transform a Collection of Picture into a List<PictureEntity>
     *
     * @param PictureCollection Collection<Picture>
     * @return List<PictureEntity>
     */
    public List<PictureEntity> transformToListPictureEntity(Collection<Picture> PictureCollection) {
        final List<PictureEntity> pictureEntityList = new ArrayList<>();
        for (Picture picture : PictureCollection) {
            final PictureEntity pictureEntity = transformToPictureEntity(picture);
            if (pictureEntity != null) {
                pictureEntityList.add(pictureEntity);
            }
        }
        return pictureEntityList;
    }

    /**
     * Create a Picture Entity from the database
     *
     * @param cursor a Database Cursor
     * @return PictureEntity
     */
    public PictureEntity createPictureEntity(Cursor cursor) {
        PictureEntity pictureEntity = new PictureEntity();
        try {
            pictureEntity.setId(cursor.getInt(cursor.getColumnIndexOrThrow(SqlLiteDatabaseHandler.COLUM_ID)));
            pictureEntity.setAlbumId(cursor.getInt(cursor.getColumnIndexOrThrow(SqlLiteDatabaseHandler.COLUM_ALBUM)));
            pictureEntity.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(SqlLiteDatabaseHandler.COLUM_TITLE)));
            pictureEntity.setUrl(cursor.getString(cursor.getColumnIndexOrThrow(SqlLiteDatabaseHandler.COLUM_URL)));
            pictureEntity.setThumbnailUrl(cursor.getString(cursor.getColumnIndexOrThrow(SqlLiteDatabaseHandler.COLUM_THUMBNAIL_URL)));
        } catch (Exception e) {
            Log.e(TAG, "Error to create Entity from localStorage");
        }
        return pictureEntity;
    }
}
