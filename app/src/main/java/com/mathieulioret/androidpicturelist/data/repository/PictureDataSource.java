package com.mathieulioret.androidpicturelist.data.repository;


import com.mathieulioret.androidpicturelist.data.entity.PictureEntity;

import java.util.List;

import io.reactivex.Observable;

public interface PictureDataSource {

    public Observable<List<PictureEntity>> loadPictures();

}
