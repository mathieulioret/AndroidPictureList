package com.mathieulioret.androidpicturelist.data.repository.local;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.mathieulioret.androidpicturelist.data.entity.PictureEntity;


public class SqlLiteDatabaseHandler extends SQLiteOpenHelper {


    public final static String TABLE_NAME = PictureEntity.class.getSimpleName();
    public final static String COLUM_ID = "id";
    public final static String COLUM_ALBUM = "albumId";
    public final static String COLUM_TITLE = "title";
    public final static String COLUM_URL = "url";
    public final static String COLUM_THUMBNAIL_URL = "thumbnailUrl";
    private final static String DB_NAME = "PicturesDatabase.db";
    private final static int DB_VERSION = 1;
    private final static String TYPE_TEXT = " TEXT";
    private final static String TYPE_INTEGER = " INTEGER";
    private final static String SEPARATOR = ", ";


    private final static String CREATE_ENTITY = "CREATE TABLE "
            + TABLE_NAME
            + " ("
            + COLUM_ID + TYPE_INTEGER + " PRIMARY KEY " + SEPARATOR
            + COLUM_ALBUM + TYPE_INTEGER + SEPARATOR
            + COLUM_TITLE + TYPE_TEXT + SEPARATOR
            + COLUM_URL + TYPE_TEXT + SEPARATOR
            + COLUM_THUMBNAIL_URL + TYPE_TEXT
            + ")";


    private final static String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    public SqlLiteDatabaseHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_ENTITY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
