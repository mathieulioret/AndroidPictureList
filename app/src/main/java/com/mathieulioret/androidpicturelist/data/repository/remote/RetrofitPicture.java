package com.mathieulioret.androidpicturelist.data.repository.remote;

import com.mathieulioret.androidpicturelist.data.entity.PictureEntity;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface RetrofitPicture {

    String BASE_URL = "http://jsonplaceholder.typicode.com";

    @GET("photos")
    Observable<List<PictureEntity>> getPictures();
}
