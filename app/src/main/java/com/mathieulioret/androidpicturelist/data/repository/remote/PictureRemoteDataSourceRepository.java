package com.mathieulioret.androidpicturelist.data.repository.remote;


import com.mathieulioret.androidpicturelist.data.entity.PictureEntity;
import com.mathieulioret.androidpicturelist.data.repository.PictureDataSource;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;


public class PictureRemoteDataSourceRepository implements PictureDataSource, RetrofitPicture {

    private static PictureRemoteDataSourceRepository INSTANCE;

    public PictureRemoteDataSourceRepository() {
    }

    public static PictureRemoteDataSourceRepository getInstance(){
        if(INSTANCE==null){
            INSTANCE = new PictureRemoteDataSourceRepository();
        }
        return INSTANCE;
    }

    @Override
    public Observable<List<PictureEntity>> getPictures() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RetrofitPicture.BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        RetrofitPicture service = retrofit.create(RetrofitPicture.class);
        return service.getPictures();
    }

    @Override
    public Observable<List<PictureEntity>> loadPictures() {
        return this.getPictures();
    }
}
