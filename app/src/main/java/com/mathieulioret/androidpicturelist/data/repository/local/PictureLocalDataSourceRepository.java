package com.mathieulioret.androidpicturelist.data.repository.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.mathieulioret.androidpicturelist.data.entity.PictureEntity;
import com.mathieulioret.androidpicturelist.data.mapper.PictureEntityMapper;
import com.mathieulioret.androidpicturelist.data.repository.PictureDataSource;
import com.mathieulioret.androidpicturelist.domain.cache.PictureCache;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;

public class PictureLocalDataSourceRepository implements PictureDataSource, PictureLocalDataSource {

    private static PictureLocalDataSourceRepository INSTANCE;

    private SqlLiteDatabaseHandler sqlLiteDatabaseHandler;

    private PictureEntityMapper pictureEntityMapper;

    private PictureCache pictureCache;

    private PictureLocalDataSourceRepository(@NonNull Context context, @NonNull PictureEntityMapper pictureEntityMapper, @NonNull PictureCache pictureCache) {
        this.sqlLiteDatabaseHandler = new SqlLiteDatabaseHandler(context);
        this.pictureEntityMapper = pictureEntityMapper;
        this.pictureCache = pictureCache;
    }

    public static PictureLocalDataSourceRepository getInstance(
            @NonNull Context context,
            @NonNull PictureEntityMapper pictureEntityMapper,
            @NonNull PictureCache pictureCache) {
        if (INSTANCE == null) {
            INSTANCE = new PictureLocalDataSourceRepository(context, pictureEntityMapper, pictureCache);
        }
        return INSTANCE;
    }

    @Override
    public Observable<List<PictureEntity>> loadPictures() {
        SQLiteDatabase db = this.sqlLiteDatabaseHandler.getReadableDatabase();

        Cursor cursor = db.query(
                SqlLiteDatabaseHandler.TABLE_NAME,   // The table to query
                null,             // The array of columns to return (pass null to get all)
                null,              // The columns for the WHERE clause
                null,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                null               // The sort order
        );

        List<PictureEntity> pictureEntities = new ArrayList<>();
        while (cursor.moveToNext()) {
            PictureEntity pictureEntity = pictureEntityMapper.createPictureEntity(cursor);
            pictureEntities.add(pictureEntity);
        }

        cursor.close();
        db.close();

        pictureCache.setCache(pictureEntities);

        return pictureCache.getCache();
    }

    @Override
    public void savePictures(final List<PictureEntity> pictureEntities) {

        Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> e) throws Exception {
                SQLiteDatabase db = sqlLiteDatabaseHandler.getWritableDatabase();

                //delete all pictures before to insert the PictureEntity List
                db.delete(SqlLiteDatabaseHandler.TABLE_NAME, null, null);
                for (PictureEntity pictureEntity : pictureEntities) {

                    ContentValues values = new ContentValues();
                    values.put(SqlLiteDatabaseHandler.COLUM_ID, pictureEntity.getId());
                    values.put(SqlLiteDatabaseHandler.COLUM_ALBUM, pictureEntity.getAlbumId());
                    values.put(SqlLiteDatabaseHandler.COLUM_TITLE, pictureEntity.getTitle());
                    values.put(SqlLiteDatabaseHandler.COLUM_THUMBNAIL_URL, pictureEntity.getThumbnailUrl());
                    values.put(SqlLiteDatabaseHandler.COLUM_URL, pictureEntity.getUrl());

                    db.insert(SqlLiteDatabaseHandler.TABLE_NAME, null, values);
                }
                db.close();
            }
        }).subscribeOn(Schedulers.computation()).subscribe();

    }


}
