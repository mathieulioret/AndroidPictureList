package com.mathieulioret.androidpicturelist.data.repository;

import android.content.Context;
import android.support.annotation.NonNull;

import com.mathieulioret.androidpicturelist.data.entity.PictureEntity;
import com.mathieulioret.androidpicturelist.data.mapper.PictureEntityMapper;
import com.mathieulioret.androidpicturelist.data.repository.local.PictureLocalDataSourceRepository;
import com.mathieulioret.androidpicturelist.data.repository.remote.PictureRemoteDataSourceRepository;
import com.mathieulioret.androidpicturelist.domain.cache.PictureCache;
import com.mathieulioret.androidpicturelist.domain.model.Picture;
import com.mathieulioret.androidpicturelist.domain.repository.PictureRepository;
import com.mathieulioret.androidpicturelist.domain.utils.Utils;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.functions.Function;


public class PictureDataRepository implements PictureRepository {

    private final Context context;
    private final PictureEntityMapper pictureEntityMapper;
    private final PictureCache pictureCache;

    @Inject
    PictureDataRepository(@NonNull Context context, @NonNull PictureCache pictureCache, @NonNull PictureEntityMapper pictureEntityMapper) {
        this.context = context.getApplicationContext();
        this.pictureEntityMapper = pictureEntityMapper;
        this.pictureCache = pictureCache;
    }

    @Override
    public Observable<List<Picture>> getPictures() {

        PictureDataSource pictureDataSource;
        PictureLocalDataSourceRepository pictureLocalDataSourceRepository
                = PictureLocalDataSourceRepository.getInstance(context, pictureEntityMapper, pictureCache);
        if (pictureCache.isCached()) {
            return pictureCache.getCache().map(
                    new Function<List<PictureEntity>, List<Picture>>() {
                        @Override
                        public List<Picture> apply(List<PictureEntity> pictureEntities) throws Exception {
                            return pictureEntityMapper.transform(pictureEntities);
                        }
                    });
        } else if (!Utils.isOnline(context)) {
            pictureDataSource = pictureLocalDataSourceRepository;
        } else {
            pictureDataSource = PictureRemoteDataSourceRepository.getInstance();
        }
        return pictureDataSource.loadPictures().map(
                new Function<List<PictureEntity>, List<Picture>>() {
                    @Override
                    public List<Picture> apply(List<PictureEntity> pictureEntities) throws Exception {
                        return pictureEntityMapper.transform(pictureEntities);
                    }
                });
    }

}
