package com.mathieulioret.androidpicturelist.data.repository.local;


import com.mathieulioret.androidpicturelist.data.entity.PictureEntity;

import java.util.List;

public interface PictureLocalDataSource {

    void savePictures(final List<PictureEntity> pictureEntities);

}
