package com.mathieulioret.androidpicturelist.presentation.view.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mathieulioret.androidpicturelist.R;
import com.mathieulioret.androidpicturelist.presentation.model.PictureModel;
import com.squareup.picasso.Picasso;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PictureAdapter extends RecyclerView.Adapter<PictureAdapter.PictureViewHolder> {

    private final LayoutInflater layoutInflater;
    private Context context;
    private List<PictureModel> pictureCollection;
    private OnItemClickListener onItemClickListener;

    @Inject
    PictureAdapter(Context context) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.pictureCollection = Collections.emptyList();
    }

    @Override
    public PictureViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = this.layoutInflater.inflate(R.layout.row_picture, parent, false);
        return new PictureViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PictureViewHolder holder, final int position) {
        final PictureModel pictureModel = this.pictureCollection.get(position);

        holder.bindPicture(pictureModel);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PictureAdapter.this.onItemClickListener != null) {
                    PictureAdapter.this.onItemClickListener.onPictureItemClicked(pictureModel);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (this.pictureCollection != null) ? this.pictureCollection.size() : 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setPictureCollection(@NonNull  Collection<PictureModel> pictureCollection) {
        this.pictureCollection = (List<PictureModel>) pictureCollection;
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onPictureItemClicked(PictureModel pictureModel);
    }

    class PictureViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView textViewTitle;

        @BindView(R.id.image)
        ImageView image;

        @BindView(R.id.pictureId)
        TextView textId;

        PictureViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bindPicture(PictureModel pictureModel) {
            textViewTitle.setText(pictureModel.getTitle());
            textId.setText(Integer.toString(pictureModel.getId()));
            Picasso.with(context)
                    .load(pictureModel.getUrl())
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(image);

        }
    }
}