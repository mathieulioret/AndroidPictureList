package com.mathieulioret.androidpicturelist.presentation.view.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mathieulioret.androidpicturelist.R;
import com.mathieulioret.androidpicturelist.presentation.Application;
import com.mathieulioret.androidpicturelist.presentation.contract.PictureContract;
import com.mathieulioret.androidpicturelist.presentation.di.component.DaggerPictureComponent;
import com.mathieulioret.androidpicturelist.presentation.di.component.PictureComponent;
import com.mathieulioret.androidpicturelist.presentation.model.PictureModel;
import com.mathieulioret.androidpicturelist.presentation.view.adapter.PictureAdapter;

import java.util.Collection;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PictureFragment extends Fragment implements PictureContract.PictureView {

    @Inject
    PictureContract.PicturePresenter picturePresenter;

    @Inject
    PictureAdapter pictureAdapter;

    @BindView(R.id.pictures)
    RecyclerView recyclerViewPictures;


    public PictureFragment() {
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PictureComponent pictureComponent = DaggerPictureComponent.builder()
                .applicationComponent(Application.app().getApplicationComponent())
                .build();
        pictureComponent.inject(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View fragmentView = inflater.inflate(R.layout.fragment_picture, container, false);
        ButterKnife.bind(this, fragmentView);
        initRecycleView();
        return fragmentView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.picturePresenter.setView(this);
        this.loadPictures();
    }


    /**
     * set pictures loaded in the PictureAdapter
     *
     * @param pictureModelCollection Collection of PictureModel
     */
    public void renderPictures(@NonNull  Collection<PictureModel> pictureModelCollection) {
        this.pictureAdapter.setPictureCollection(pictureModelCollection);
    }

    public void loadPictures() {
        this.picturePresenter.loadPictures();
    }

    private void initRecycleView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getActivity().getApplicationContext());
        this.recyclerViewPictures.setLayoutManager(layoutManager);
        this.recyclerViewPictures.setAdapter(pictureAdapter);
        this.recyclerViewPictures.setHasFixedSize(true);
    }

}
