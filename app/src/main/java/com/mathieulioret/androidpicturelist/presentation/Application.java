package com.mathieulioret.androidpicturelist.presentation;


import com.mathieulioret.androidpicturelist.presentation.di.component.ApplicationComponent;
import com.mathieulioret.androidpicturelist.presentation.di.component.DaggerApplicationComponent;
import com.mathieulioret.androidpicturelist.presentation.di.module.ApplicationModule;

public class Application extends android.app.Application {

    private static Application application;
    private ApplicationComponent applicationComponent;

    public static Application app() {
        return application;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initializeInjector();
        application = this;
    }

    private void initializeInjector() {

        this.applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }
}
