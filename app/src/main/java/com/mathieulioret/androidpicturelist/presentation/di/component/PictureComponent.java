package com.mathieulioret.androidpicturelist.presentation.di.component;


import com.mathieulioret.androidpicturelist.domain.repository.PictureRepository;
import com.mathieulioret.androidpicturelist.presentation.contract.PictureContract;
import com.mathieulioret.androidpicturelist.presentation.di.CustomScope;
import com.mathieulioret.androidpicturelist.presentation.di.module.PictureModule;
import com.mathieulioret.androidpicturelist.presentation.view.fragment.PictureFragment;

import dagger.Component;

@CustomScope
@Component(dependencies = ApplicationComponent.class, modules = PictureModule.class)
public interface PictureComponent {

    void inject(PictureFragment pictureFragment);

    PictureRepository pictureRepository();

}


