package com.mathieulioret.androidpicturelist.presentation.view.activity;


import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.mathieulioret.androidpicturelist.presentation.Application;
import com.mathieulioret.androidpicturelist.presentation.view.fragment.PictureFragment;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((Application)getApplication()).getApplicationComponent().inject(this);

    }

    protected void addFragment(int id, PictureFragment fragment){
        final FragmentTransaction fragmentTransaction = this.getFragmentManager().beginTransaction();
        fragmentTransaction.add(id, fragment);
        fragmentTransaction.commit();
    }

}
