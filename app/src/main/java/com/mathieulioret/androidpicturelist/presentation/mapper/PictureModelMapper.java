package com.mathieulioret.androidpicturelist.presentation.mapper;


import android.support.annotation.NonNull;

import com.mathieulioret.androidpicturelist.domain.model.Picture;
import com.mathieulioret.androidpicturelist.presentation.model.PictureModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

/**
 * map PictureModel to Picture (Presentation To Domain)
 */
public class PictureModelMapper {

    @Inject
    PictureModelMapper() {
    }

    /**
     * Transform a Picture into PictureModel
     *
     * @param picture Picture
     * @return PictureModel
     */
    public PictureModel transform(@NonNull Picture picture) {
        PictureModel pictureModel = new PictureModel();
        pictureModel.setId(picture.getId());
        pictureModel.setAlbumId(picture.getAlbumId());
        pictureModel.setThumbnailUrl(picture.getThumbnailUrl());
        pictureModel.setTitle(picture.getTitle());
        pictureModel.setUrl(picture.getUrl());
        return pictureModel;
    }

    /**
     * Transform a Collection of Pictureinto a List<PictureModel>
     *
     * @param pictureCollection Collection<Picture>
     * @return List<PictureModel>
     */
    public List<PictureModel> transform(Collection<Picture> pictureCollection) {
        final List<PictureModel> pictureModelList = new ArrayList<>();
        for (Picture picture : pictureCollection) {
            final PictureModel pictureModel = transform(picture);
            if (pictureModel != null) {
                pictureModelList.add(pictureModel);
            }
        }
        return pictureModelList;
    }
}
