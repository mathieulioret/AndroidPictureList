package com.mathieulioret.androidpicturelist.presentation.view.activity;

import android.os.Bundle;

import com.mathieulioret.androidpicturelist.R;
import com.mathieulioret.androidpicturelist.presentation.view.fragment.PictureFragment;

public class PictureActivity extends BaseActivity {

    private static final String FRAGMENT_INSTANCE = "pictureFragment";
    PictureFragment pictureFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pictures);
        if (savedInstanceState == null) {
            pictureFragment = new PictureFragment();
            addFragment(R.id.fragmentContainer, pictureFragment);
        } else {
            pictureFragment = (PictureFragment) getFragmentManager().getFragment(savedInstanceState,
                    FRAGMENT_INSTANCE);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getFragmentManager().putFragment(outState, FRAGMENT_INSTANCE, pictureFragment);
    }
}
