package com.mathieulioret.androidpicturelist.presentation.presenter;

import android.util.Log;

import com.mathieulioret.androidpicturelist.domain.cache.PictureCache;
import com.mathieulioret.androidpicturelist.domain.model.Picture;
import com.mathieulioret.androidpicturelist.domain.repository.PictureRepository;
import com.mathieulioret.androidpicturelist.presentation.contract.PictureContract;
import com.mathieulioret.androidpicturelist.presentation.mapper.PictureModelMapper;
import com.mathieulioret.androidpicturelist.presentation.model.PictureModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


public class PicturePresenterImpl implements PictureContract.PicturePresenter {

    private final PictureModelMapper pictureModelMapper;
    private final PictureCache pictureCache;
    PictureContract.PictureView pictureView;
    private PictureRepository pictureRepository;
    Collection<PictureModel> pictureModelCollection;

    @Inject
    PicturePresenterImpl(PictureRepository pictureRepository, PictureModelMapper pictureModelMapper, PictureCache pictureCache) {
        this.pictureRepository = pictureRepository;
        this.pictureModelMapper = pictureModelMapper;
        this.pictureCache = pictureCache;
        this.pictureModelCollection = new ArrayList<PictureModel>();
    }

    @Override
    public void loadPictures() {
        if(pictureCache.isModelCached()){
            pictureView.renderPictures(pictureCache.getCachePictureModel());
        }
        else {
            this.pictureRepository.getPictures().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<List<Picture>>() {
                        @Override
                        public void accept(List<Picture> pictureList) throws Exception {
                            pictureModelCollection = pictureModelMapper.transform(pictureList);
                            pictureCache.setCachePictureModel(pictureModelCollection);
                            if(!pictureCache.isCached()) {
                                pictureCache.setCachePicture(pictureList);
                            }
                            pictureView.renderPictures(pictureModelCollection);
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.d("test", throwable.toString());
                        }
                    });
        }

    }

    @Override
    public void setView(PictureContract.PictureView pictureView) {
        this.pictureView = pictureView;
    }
}
