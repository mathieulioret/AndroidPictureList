package com.mathieulioret.androidpicturelist.presentation.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * To fix unscoped Module and component
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface CustomScope {
}