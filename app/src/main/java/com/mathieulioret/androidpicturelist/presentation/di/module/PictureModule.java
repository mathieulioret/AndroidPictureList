package com.mathieulioret.androidpicturelist.presentation.di.module;

import com.mathieulioret.androidpicturelist.data.repository.PictureDataRepository;
import com.mathieulioret.androidpicturelist.domain.repository.PictureRepository;
import com.mathieulioret.androidpicturelist.presentation.contract.PictureContract;
import com.mathieulioret.androidpicturelist.presentation.presenter.PicturePresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class PictureModule {

    public PictureModule() {
    }

    @Provides
    PictureRepository providePictureRepository(PictureDataRepository pictureRepository) {
        return pictureRepository;
    }

    @Provides
    PictureContract.PicturePresenter providePicturePresenter(PicturePresenterImpl picturePresenterImpl) {
        return picturePresenterImpl;
    }
}
