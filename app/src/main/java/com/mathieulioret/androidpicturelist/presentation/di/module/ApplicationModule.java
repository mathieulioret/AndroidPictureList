package com.mathieulioret.androidpicturelist.presentation.di.module;

import android.content.Context;

import com.mathieulioret.androidpicturelist.domain.cache.PictureCache;
import com.mathieulioret.androidpicturelist.domain.cache.PictureCacheImpl;
import com.mathieulioret.androidpicturelist.presentation.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return this.application;
    }

    @Singleton
    @Provides
    PictureCache providePictureCache(PictureCacheImpl pictureCacheImpl) {
        return pictureCacheImpl;
    }

}
