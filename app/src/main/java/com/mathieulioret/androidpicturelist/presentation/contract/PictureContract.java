package com.mathieulioret.androidpicturelist.presentation.contract;

import com.mathieulioret.androidpicturelist.presentation.model.PictureModel;

import java.util.Collection;

public interface PictureContract {


    interface PicturePresenter {

        /**
         * Load pictures from Database or remote Datasource
         */
        void loadPictures();

        /**
         * Set View
         * @param view PictureView (Fragment)
         */
        void setView(PictureView view);

    }

    interface PictureView {

        /**
         * show the pictures
         * @param pictureModelCollection Collection<PictureModel>
         */
        void renderPictures(Collection<PictureModel> pictureModelCollection);

        /**
         * Load pictures from the presenter
         */
        void loadPictures();

    }


}
