package com.mathieulioret.androidpicturelist.presentation.di.component;

import android.content.Context;

import com.mathieulioret.androidpicturelist.domain.cache.PictureCache;
import com.mathieulioret.androidpicturelist.presentation.di.module.ApplicationModule;
import com.mathieulioret.androidpicturelist.presentation.view.activity.BaseActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(BaseActivity baseActivity);

    Context context();

    PictureCache pictureCache();
}
