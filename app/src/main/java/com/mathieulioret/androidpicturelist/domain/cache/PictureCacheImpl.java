package com.mathieulioret.androidpicturelist.domain.cache;

import android.content.Context;

import com.mathieulioret.androidpicturelist.data.entity.PictureEntity;
import com.mathieulioret.androidpicturelist.data.mapper.PictureEntityMapper;
import com.mathieulioret.androidpicturelist.data.repository.local.PictureLocalDataSourceRepository;
import com.mathieulioret.androidpicturelist.domain.model.Picture;
import com.mathieulioret.androidpicturelist.presentation.model.PictureModel;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

@Singleton
public class PictureCacheImpl implements PictureCache {

    // Cache Data Layer
    private BehaviorSubject<List<PictureEntity>> pictureEntitiesCache;

    // Cache Presentation Layer
    private BehaviorSubject<Collection<PictureModel>> pictureModelCache;

    private PictureLocalDataSourceRepository pictureLocalDataSource;

    private PictureEntityMapper pictureEntityMapper;

    @Inject
    public PictureCacheImpl(Context context, PictureEntityMapper pictureEntityMapper) {
        pictureEntitiesCache = BehaviorSubject.create();
        pictureModelCache = BehaviorSubject.create();
        this.pictureEntityMapper = pictureEntityMapper;
        pictureLocalDataSource = PictureLocalDataSourceRepository.getInstance(context, pictureEntityMapper, this);
    }

    /**
     * isCached Data Layer
     * @return boolean
     */
    @Override
    public boolean isCached() {
        return pictureEntitiesCache.hasValue();
    }

    /**
     *  isCached Presentation Layer
     * @return boolean
     */
    @Override
    public boolean isModelCached() {
        return pictureModelCache.hasValue();
    }

    /**
     * get cache Data Layer
     * @return Observable<List<PictureEntity>>
     */
    @Override
    public Observable<List<PictureEntity>> getCache() {
        return pictureEntitiesCache;
    }

    /**
     * get cache Presentation Layer
     * @return Collection<PictureModel>
     */
    @Override
    public Collection<PictureModel> getCachePictureModel() {
        return pictureModelCache.getValue();
    }

    public void setCache(List<PictureEntity> pictureEntities) {
        pictureEntitiesCache.onNext(pictureEntities);
        //save pictures in local Database
        pictureLocalDataSource.savePictures(pictureEntities);
    }

    @Override
    public void setCachePicture(List<Picture> pictures) {
        setCache(pictureEntityMapper.transformToListPictureEntity(pictures));
    }

    @Override
    public void setCachePictureModel(Collection<PictureModel> pictureModel) {
        pictureModelCache.onNext(pictureModel);
    }

}
