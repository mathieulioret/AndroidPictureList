package com.mathieulioret.androidpicturelist.domain.repository;


import com.mathieulioret.androidpicturelist.domain.model.Picture;

import java.util.List;

import io.reactivex.Observable;

public interface PictureRepository {

    Observable<List<Picture>> getPictures();

}
