package com.mathieulioret.androidpicturelist.domain.cache;


import com.mathieulioret.androidpicturelist.data.entity.PictureEntity;
import com.mathieulioret.androidpicturelist.domain.model.Picture;
import com.mathieulioret.androidpicturelist.presentation.model.PictureModel;

import java.util.Collection;
import java.util.List;

import io.reactivex.Observable;


public interface PictureCache {

    /**
     * isCached Data Layer
     * @return boolean
     */
    boolean isCached();

    /**
     *  isCached Presentation Layer
     * @return boolean
     */
    boolean isModelCached();

    void setCachePicture(List<Picture> pictures);

    void setCachePictureModel(Collection<PictureModel> pictureModel);

    /**
     * get cache Data Layer
     * @return Observable<List<PictureEntity>>
     */
    Observable<List<PictureEntity>> getCache();

    /**
     * get cache Presentation Layer
     * @return Collection<PictureModel>
     */
    Collection<PictureModel> getCachePictureModel();

    void setCache(List<PictureEntity> pictureEntities);
}
