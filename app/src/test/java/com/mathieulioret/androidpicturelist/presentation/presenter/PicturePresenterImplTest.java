package com.mathieulioret.androidpicturelist.presentation.presenter;

import com.mathieulioret.androidpicturelist.domain.cache.PictureCache;
import com.mathieulioret.androidpicturelist.domain.repository.PictureRepository;
import com.mathieulioret.androidpicturelist.presentation.contract.PictureContract;
import com.mathieulioret.androidpicturelist.presentation.mapper.PictureModelMapper;
import com.mathieulioret.androidpicturelist.presentation.model.PictureModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collection;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PicturePresenterImplTest {

    @Mock
    private PictureModelMapper pictureModelMapper;

    @Mock
    private PictureCache pictureCache;

    @Mock
    private PictureContract.PictureView pictureView;

    @Mock
    private PictureRepository pictureRepository;


    private Collection<PictureModel> pictureModelCollection;

    private PicturePresenterImpl picturePresenter;


    @Before
    public void setUp() throws Exception {
        picturePresenter = new PicturePresenterImpl(pictureRepository, pictureModelMapper, pictureCache);
        pictureModelCollection = new ArrayList<>();
        pictureModelCollection.add(mock(PictureModel.class));
    }

    @Test
    public void shouldLoadPicturesWithCache() throws Exception {
        //GIVEN
        picturePresenter.setView(pictureView);
        //WHEN
        when(pictureCache.isModelCached()).thenReturn(true);
        when(pictureCache.getCachePictureModel()).thenReturn(pictureModelCollection);
        picturePresenter.loadPictures();
        //THEN
        //no error
        assertTrue(true);
    }

    @Test
    public void shouldSetView() throws Exception {
        assertNull(picturePresenter.pictureView);
        picturePresenter.setView(pictureView);
        assertNotNull(picturePresenter.pictureView);
    }

}