package com.mathieulioret.androidpicturelist.presentation.mapper;

import com.mathieulioret.androidpicturelist.domain.model.Picture;
import com.mathieulioret.androidpicturelist.presentation.model.PictureModel;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import static org.junit.Assert.*;


public class PictureModelMapperTest {

    private Picture picture;
    private PictureModel pictureModel;

    private PictureModelMapper pictureModelMapper;


    @Before
    public void setUp() throws Exception {

        pictureModelMapper = new PictureModelMapper();

        //init picture
        picture = new Picture();
        picture.setUrl("url");
        picture.setTitle("title");
        picture.setId(1);
        picture.setAlbumId(1);
        picture.setThumbnailUrl("thumbnailUrl");

        //init pictureModel
        pictureModel = new PictureModel();
        pictureModel.setUrl("url");
        pictureModel.setTitle("title");
        pictureModel.setId(1);
        pictureModel.setAlbumId(1);
        pictureModel.setThumbnailUrl("thumbnailUrl");
    }

    @Test
    public void shouldTotransformPictureToPictureModel() throws Exception {
        //GIVEN
        //WHEN
        PictureModel pictureModelTransformed = pictureModelMapper.transform(picture);
        //THEN
        assertEquals( pictureModel, pictureModelTransformed);
    }

    @Test
    public void shouldTotransformPictureCollectionToListPictureModel() throws Exception {
        //GIVEN
        Collection<Picture> pictureCollection = new ArrayList<Picture>();
        pictureCollection.add(picture);
        //WHEN
        List<PictureModel> pictureModelListTransformed = pictureModelMapper.transform(pictureCollection);
        //THEN
        assertEquals(1, pictureModelListTransformed.size());
        assertEquals(pictureModel, pictureModelListTransformed.get(0));

    }

}