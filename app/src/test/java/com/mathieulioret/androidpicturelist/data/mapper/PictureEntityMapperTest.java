package com.mathieulioret.androidpicturelist.data.mapper;

import com.mathieulioret.androidpicturelist.data.entity.PictureEntity;
import com.mathieulioret.androidpicturelist.domain.model.Picture;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;

public class PictureEntityMapperTest {

    private Picture picture;
    private PictureEntity pictureEntity;

    private PictureEntityMapper pictureEntityMapper;

    @Before
    public void setUp() throws Exception {

        pictureEntityMapper = new PictureEntityMapper();

        //init picture
        picture = new Picture();
        picture.setUrl("url");
        picture.setTitle("title");
        picture.setId(1);
        picture.setAlbumId(1);
        picture.setThumbnailUrl("thumbnailUrl");

        //init pictureEntity
        pictureEntity = new PictureEntity();
        pictureEntity.setUrl("url");
        pictureEntity.setTitle("title");
        pictureEntity.setId(1);
        pictureEntity.setAlbumId(1);
        pictureEntity.setThumbnailUrl("thumbnailUrl");
    }

    @Test
    public void shouldTotransformPictureEntityToPicture() throws Exception {
        //GIVEN
        //WHEN
        Picture pictureTransformed = pictureEntityMapper.transform(pictureEntity);
        //THEN
        assertEquals( picture, pictureTransformed);
    }

    @Test
    public void shouldTotransformPictureToPictureEntity() throws Exception {
        //GIVEN
        //WHEN
        PictureEntity pictureEntityTransformed = pictureEntityMapper.transformToPictureEntity(picture);
        //THEN
        assertEquals( pictureEntity, pictureEntityTransformed);
    }

    @Test
    public void shouldTotransformPictureEntityCollectionToListPicture() throws Exception {
        //GIVEN
        Collection<PictureEntity> pictureCollection = new ArrayList<PictureEntity>();
        pictureCollection.add(pictureEntity);
        //WHEN
        List<Picture> pictureListTransformed = pictureEntityMapper.transform(pictureCollection);
        //THEN
        assertEquals(1, pictureListTransformed.size());
        assertEquals(picture, pictureListTransformed.get(0));
    }

    @Test
    public void shouldTotransformPictureCollectionToListPictureEntity() throws Exception {
        //GIVEN
        Collection<Picture> pictureCollection = new ArrayList<Picture>();
        pictureCollection.add(picture);
        //WHEN
        List<PictureEntity> pictureListTransformed = pictureEntityMapper.transformToListPictureEntity(pictureCollection);
        //THEN
        assertEquals(1, pictureListTransformed.size());
        assertEquals(pictureEntity, pictureListTransformed.get(0));
    }

}