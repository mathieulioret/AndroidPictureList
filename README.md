# Android Picture List

### Description

It is an Android application that show a list of pictures from : http://jsonplaceholder.typicode.com/photos

### Content :
- Clean Architecture
- MVP Implementation
- API Min : 14
- Offline and Online Mode 
- Cache
- Rotation ( configuration changed)
- Dependency Injection
- RecyclerView

#### Library :
 - Retrofit
 - RxJava
 - SQLite Database
 - Dagger 2
 - TestUnit with Mockito and jUnit
 - Picasso 
 - Butterknife
 - mockito 
 - espresso
